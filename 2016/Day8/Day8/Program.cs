﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day8
{
    class Program
    {
        private static string sample = @"rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4
rotate column x=1 by 1";

        static void Main(string[] args)
        {
            //int cols = 7;
            //int rows = 3;
            int cols = 50;
            int rows = 6;

            var theGrid = new Light[cols, rows];

            for (int i = 0; i < cols; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    theGrid[i, j] = new Light();
                }
            }

            int flippedOn = 0;

            foreach (var line in inputString.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                if (line.StartsWith("rect"))
                {
                    var vals = line.Split(new char[] {' ', 'x'});
                    var x = int.Parse(vals[1]);
                    var y = int.Parse(vals[2]);

                    for (int j = 0; j < y; j++)
                    {
                        for (int i = 0; i < x; i++)
                        {
                            theGrid[i, j].IsOn = true;
                            flippedOn++;
                        }
                    }
                }
                else if (line.StartsWith("rotate column"))
                {
                    var vals = line.Split(new char[] {' ', '='});
                    var col = int.Parse(vals[3]);
                    var amt = int.Parse(vals[5]);

                    var stk = new List<bool>();

                    for (int y = 0; y < rows; y++)
                    {
                        stk.Add(theGrid[col, y].IsOn);
                    }

                    for (int i = 0; i < amt; i++)
                    {
                        var l = stk.Last();
                        stk.RemoveAt(rows-1);
                        stk.Insert(0, l);
                    }

                    for (int y = 0; y < rows; y++)
                    {
                        theGrid[col, y].IsOn = stk[y];
                    }
                }
                else if (line.StartsWith("rotate row"))
                {
                    var vals = line.Split(new char[] {' ', '='});
                    var row = int.Parse(vals[3]);
                    var amt = int.Parse(vals[5]);

                    var stk = new List<bool>();

                    for (int x = 0; x < cols; x++)
                    {
                        stk.Add(theGrid[x, row].IsOn);
                    }

                    for (int i = 0; i < amt; i++)
                    {
                        var l = stk.Last();
                        stk.RemoveAt(cols-1);
                        stk.Insert(0, l);
                    }

                    for (int x = 0; x < cols; x++)
                    {
                        theGrid[x, row].IsOn = stk.ElementAt(x);
                    }
                }
            }

            var lightOn = 0;

            for (int j = 0; j < rows; j++)
            {
                for (int i = 0; i < cols; i++)
                {
                    if (theGrid[i, j].IsOn)
                    {
                        Console.Write("#");
                        lightOn++;
                    }
                    else
                    {
                        Console.Write(".");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine(lightOn);
            Console.WriteLine(flippedOn);
            Console.ReadLine();
        }

        private static string inputString = @"rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 3
rect 2x1
rotate row y=0 by 2
rect 1x2
rotate row y=1 by 5
rotate row y=0 by 3
rect 1x2
rotate column x=30 by 1
rotate column x=25 by 1
rotate column x=10 by 1
rotate row y=1 by 5
rotate row y=0 by 2
rect 1x2
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=2 by 18
rotate row y=0 by 5
rotate column x=0 by 1
rect 3x1
rotate row y=2 by 12
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate column x=20 by 1
rotate row y=2 by 5
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=2 by 15
rotate row y=0 by 15
rotate column x=10 by 1
rotate column x=5 by 1
rotate column x=0 by 1
rect 14x1
rotate column x=37 by 1
rotate column x=23 by 1
rotate column x=7 by 2
rotate row y=3 by 20
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=3 by 5
rotate row y=2 by 2
rotate row y=1 by 4
rotate row y=0 by 4
rect 1x4
rotate column x=35 by 3
rotate column x=18 by 3
rotate column x=13 by 3
rotate row y=3 by 5
rotate row y=2 by 3
rotate row y=1 by 1
rotate row y=0 by 1
rect 1x5
rotate row y=4 by 20
rotate row y=3 by 10
rotate row y=2 by 13
rotate row y=0 by 10
rotate column x=5 by 1
rotate column x=3 by 3
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate row y=4 by 10
rotate row y=3 by 10
rotate row y=1 by 10
rotate row y=0 by 10
rotate column x=7 by 2
rotate column x=5 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate row y=4 by 20
rotate row y=3 by 12
rotate row y=1 by 15
rotate row y=0 by 10
rotate column x=8 by 2
rotate column x=7 by 1
rotate column x=6 by 2
rotate column x=5 by 1
rotate column x=3 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate column x=46 by 2
rotate column x=43 by 2
rotate column x=24 by 2
rotate column x=14 by 3
rotate row y=5 by 15
rotate row y=4 by 10
rotate row y=3 by 3
rotate row y=2 by 37
rotate row y=1 by 10
rotate row y=0 by 5
rotate column x=0 by 3
rect 3x3
rotate row y=5 by 15
rotate row y=3 by 10
rotate row y=2 by 10
rotate row y=0 by 10
rotate column x=7 by 3
rotate column x=6 by 3
rotate column x=5 by 1
rotate column x=3 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate column x=19 by 1
rotate column x=10 by 3
rotate column x=5 by 4
rotate row y=5 by 5
rotate row y=4 by 5
rotate row y=3 by 40
rotate row y=2 by 35
rotate row y=1 by 15
rotate row y=0 by 30
rotate column x=48 by 4
rotate column x=47 by 3
rotate column x=46 by 3
rotate column x=45 by 1
rotate column x=43 by 1
rotate column x=42 by 5
rotate column x=41 by 5
rotate column x=40 by 1
rotate column x=33 by 2
rotate column x=32 by 3
rotate column x=31 by 2
rotate column x=28 by 1
rotate column x=27 by 5
rotate column x=26 by 5
rotate column x=25 by 1
rotate column x=23 by 5
rotate column x=22 by 5
rotate column x=21 by 5
rotate column x=18 by 5
rotate column x=17 by 5
rotate column x=16 by 5
rotate column x=13 by 5
rotate column x=12 by 5
rotate column x=11 by 5
rotate column x=3 by 1
rotate column x=2 by 5
rotate column x=1 by 5
rotate column x=0 by 1";
    }

    class Light
    {
        public Light()
        {
        }

        public bool IsOn { get; set; }
    }
}