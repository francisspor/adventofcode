﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15
{
    class Disk
    {
        public int ID;
        public int POS_TOTAL;
        public int POS_START;

        public bool isSpace(int Time, int DiskNum, int Total, int Start)
        {
            if ((Time + DiskNum + Start) % Total == 0)
                return true;

            return false;
        }
    }

    class Program
    {
        private static string inputString = @"Disc #1 has 13 positions; at time=0, it is at position 11.
Disc #2 has 5 positions; at time=0, it is at position 0.
Disc #3 has 17 positions; at time=0, it is at position 11.
Disc #4 has 3 positions; at time=0, it is at position 0.
Disc #5 has 7 positions; at time=0, it is at position 2.
Disc #6 has 19 positions; at time=0, it is at position 17.";

        static void Main(string[] args)
        {
            start();

            start(true);

            Console.ReadKey();
        }

        static void start(bool part2 = false)
        {
            string[] input = inputString.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<Disk> disksList = new List<Disk>();

            foreach (var line in input)
            {
                var str = line.Split();

                Disk dsk = new Disk();
                dsk.POS_TOTAL = Convert.ToInt32(str[3]);

                var posStart = str[11].Replace(".", "");

                dsk.POS_START = Convert.ToInt32(posStart);
                dsk.ID = Convert.ToInt32(str[1].Substring(1, 1));
                disksList.Add(dsk);
            }

            if (part2)
            {
                Disk dsk = new Disk();
                dsk.POS_TOTAL = 11;
                dsk.POS_START = 0;
                dsk.ID = disksList.Count + 1;
                disksList.Add(dsk);
            }

            int time = 0;
            bool working = true;
            while (working)
            {
                foreach (var disk in disksList)
                {
                    int time1 = time;
                    if (disk.isSpace(time1, disk.ID, disk.POS_TOTAL, disk.POS_START))
                    {
                        time1++;
                        if (disk.ID == disksList.Count)
                        {
                            working = false;
                            Console.WriteLine("Done on time: {0}", time);
                        }
                    }
                    else
                    {
  //                      Console.WriteLine("[Start Time: {0}]  Capsule bounced to Disk #{1}", time, disk.ID);
                        break;
                    }
                }

                time++;
            }
        }
    }
}
