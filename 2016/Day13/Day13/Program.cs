﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

class Program
{
    private static Size[] dirs;
    private static Dictionary<Point, int> shortest = new Dictionary<Point, int>();

    static void Main(string[] args)
    {
        int width = 150, height = 150;
        var map = new char[width, height];

        for (int y = 0; y < width; y++)
        {
            for (int x = 0; x < height; x++)
            {
                var e = x * x + 3 * x + 2 * x * y + y + y * y + 1352;

                var open = (Convert.ToString(e, 2).ToArray().Count(c => c == '1') & 1) == 0;

                map[y, x] = open ? '.' : '#';
            }
        }

        dirs = new[] { new Size(-1, 0), new Size(1, 0), new Size(0, -1), new Size(0, 1) };

        var target = new Point(31, 39);
        var start = new Point(1, 1);

        int count = -1;
        Find(map, start, target, count);

//        Console.WriteLine("Shortest:" + shortest[target]);
        Console.WriteLine("Locations:" + shortest.Count);
        Console.ReadKey();
    }

    private static void Find(char[,] map, Point current, Point target, int numSteps)
    {
        ++numSteps;

        if (numSteps >= 50)
        {
            shortest[current] = numSteps;
            return;
        }

        //if (current == target)
        //{
        //    shortest[current] = numSteps;
        //    return;
        //}

        if (shortest.ContainsKey(current))
        {
            if (shortest[current] < numSteps)
            {
                Console.WriteLine("dead end:" + shortest[current]);
                return;
            }
            shortest[current] = numSteps;
        }
        else
            shortest.Add(current, numSteps);

        if (current == new Point(1, 1))
            numSteps = 0;

        for (int i = 0; i < 4; i++)
        {
            var nm = Point.Add(current, dirs[i]);

            if (nm.X >= 0 && nm.Y >= 0 && map[nm.Y, nm.X] == '.')
            {
                Find(map, nm, target, numSteps);
            }
        }
    }
}