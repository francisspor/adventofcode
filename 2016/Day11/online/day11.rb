Gen = Struct.new(:i, :name) do
  alias to_s name
  alias inspect name
  alias :== :equal?
  def <=>(o)
    Mic === o ? 1 : i <=> o.i
  end
end
Mic = Struct.new(:i, :name) do
  alias to_s name
  alias inspect name
  alias :== :equal?
  def <=>(o)
    Gen === o ? -1 : i <=> o.i
  end
end

class Array
  def - o
    copy = dup
    o.each { |e| copy.delete(e) }
    copy
  end
end

can_be_at_same_level = -> items {
  micros, gens = items.partition { |e| Mic === e }
  return true if gens.empty?
  micros.all? { |m|
    gens.any? { |g| g.i == m.i }
  }
}

# init_state = [
#   [Mic.new(1, "HM"), Mic.new(2, "LM")],
#   [Gen.new(1, "HG")],
#   [Gen.new(2, "LG")],
#   [],
# ].map(&:sort).freeze

init_state = [
  [Gen.new(1, "PG"), Gen.new(2, "TG"), Mic.new(2, "TM"), Gen.new(3, "XG"), Gen.new(4, "RG"),Mic.new(4, "RM"), Gen.new(5, "CG"), Mic.new(5, "CM")],
  [Mic.new(1, "PM"), Mic.new(3, "XM")],
  [],
  []
]
init_state[0] += [
  Gen.new(6, "EG"), Mic.new(6, "EM"),
  Gen.new(7, "DG"), Mic.new(7, "DM")
]
init_state = init_state.map(&:sort).freeze

n_items = init_state.map(&:size).reduce(:+)

# [level, state]
init = [0, init_state].freeze
q = [init]
dists = Hash.new(Float::INFINITY)
dists[init] = 0

while current = q.shift
  level, state = current
  dist = dists[current]
  p dist
  p current + [dist]
  items = state[level]
  if level == 3 and items.size == n_items
    p dist
    exit
  end
  moves = items.map { |e| [e] } + items.combination(2).to_a
  possible_moves = moves.select { |move|
    old_level = (items - move)
    can_be_at_same_level[old_level]
  }

  moves = possible_moves

  [-1,1].each { |dir|
    l = level+dir
    if l.between?(0,3)
      moves.each { |move|
        new_level = state[l] + move
        if can_be_at_same_level[new_level]
          new_state = state.map(&:dup)
          new_state[level] = (items - move).sort
          new_state[l] = new_level.sort
          go = [l, new_state.freeze].freeze
          if dists[go] == Float::INFINITY
            dists[go] = dist+1
            q << go
          end
        end
      }
    end
  }
end