﻿using System;
using System.Collections.Generic;

namespace Day11
{
    class Program
    {
        const string downCommand = "DOWN";
        const string upCommand = "UP";
        const string elevator = "EE";
        const string poloniumGenerator= "PG";
        const string poloniumMicrochip = "PM";
        const string thuliumGenerator = "TG";
        const string thuliumMicrochip = "TM";
        const string promethiumGenerator = "XG";
        const string promethiumMicrochip = "XM";
        const string rutheniumGenerator = "RG";
        const string rutheniumMicrochip = "RM";
        const string cobaltGenerator = "CG";
        const string cobaltMicrochip = "CM";
        const string empty = "--";
        public static List<List<string>> building;
        public static int currentFloor = 0;
        public static int totalMoves = 0;
        public static Cargo[] cargoList = new Cargo[14];

        internal static void Main()
        {
            building = new List<List<string>>();
            for (int index = 0; index < 4; index++)
            {
                building.Add(new List<string>());
                for (int stringIndex = 0; stringIndex < 15; stringIndex++)
                {
                    building[index].Add(empty);
                }
            }
            building[0][0] = elevator;
            cargoList[0] = new Cargo(poloniumGenerator, 0);
            cargoList[1] = new Cargo(thuliumGenerator, 0);
            cargoList[2] = new Cargo(thuliumMicrochip, 0);
            cargoList[3] = new Cargo(promethiumGenerator, 0);
            cargoList[4] = new Cargo(rutheniumGenerator, 0);
            cargoList[5] = new Cargo(rutheniumMicrochip, 0);
            cargoList[6] = new Cargo(cobaltGenerator, 0);
            cargoList[7] = new Cargo(cobaltMicrochip, 0);
            cargoList[8] = new Cargo(poloniumMicrochip, 1);
            cargoList[9] = new Cargo(promethiumMicrochip, 1);

            while (true)
            {
                Draw();
                string command = Console.ReadLine().ToUpper();
                if (command == upCommand || command == downCommand)
                {
                    Console.Write("Cargo One: ");
                    Cargo cargoOne = Cargo.Find(Console.ReadLine());
                    Console.Write("Cargo Two: ");
                    Cargo cargoTwo = Cargo.Find(Console.ReadLine());
                    if ((cargoOne == null || cargoOne.myFloor == currentFloor) && (cargoTwo == null || cargoTwo.myFloor == currentFloor))
                    {
                        MoveCargo(cargoOne, cargoTwo, command);
                    }
                }
                else if (command == "-")
                {
                    totalMoves--;
                }
                else if (command == "+")
                {
                    totalMoves++;
                }
            }
        }

        private static void Draw()
        {
            Console.Clear();
            Console.WriteLine("Advent of Code 2016");
            Console.WriteLine();
            for (int index = 3; index >= 0; index--)
            {
                Console.Write("F" + index + " ");
                for (int stringIndex = 0; stringIndex < 15; stringIndex++)
                {
                    if (stringIndex < 14)
                    {
                        Console.Write(building[index][stringIndex] + " ");
                    }
                    else
                    {
                        Console.WriteLine(building[index][stringIndex]);
                    }
                }
            }
            Console.WriteLine("Moves: " + totalMoves);
        }

        private static void MoveCargo(Cargo cargoOne, Cargo cargoTwo, string command)
        {
            if (command.Equals(upCommand))
            {
                if (currentFloor < 3 && (cargoOne != null || cargoTwo != null))
                {
                    if (cargoOne != null)
                    {
                        cargoOne.MoveUp();
                    }

                    if (cargoTwo != null)
                    {
                        cargoTwo.MoveUp();
                    }

                    building[currentFloor][0] = empty;
                    currentFloor++;
                    building[currentFloor][0] = elevator;
                    totalMoves++;
                }
            }
            else
            {
                if (currentFloor > 0 && (cargoOne != null || cargoTwo != null))
                {
                    if (cargoOne != null)
                    {
                        cargoOne.MoveDown();
                    }

                    if (cargoTwo != null)
                    {
                        cargoTwo.MoveDown();
                    }

                    building[currentFloor][0] = empty;
                    currentFloor--;
                    building[currentFloor][0] = elevator;
                    totalMoves++;
                }
            }
        }

        public class Cargo
        {
            static int lastSlot = 0;
            public int mySlot;
            public int myFloor;
            public string myID;

            public Cargo(string id, int floor)
            {
                myID = id;
                myFloor = floor;
                lastSlot++;
                mySlot = lastSlot;
                building[myFloor][mySlot] = myID;
            }

            public void MoveDown()
            {
                building[myFloor][mySlot] = empty;
                myFloor--;
                building[myFloor][mySlot] = myID;
            }

            public void MoveUp()
            {
                building[myFloor][mySlot] = empty;
                myFloor++;
                building[myFloor][mySlot] = myID;
            }

            public static Cargo Find(string id)
            {
                foreach (Cargo cargo in cargoList)
                {
                    if (cargo.myID.ToUpper() == id.ToUpper())
                    {
                        return cargo;
                    }
                }
                return null;
            }
        }
    }
}