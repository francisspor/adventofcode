﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    class Program
    {
        private static string inputString =
            @"UDRLRRRUULUUDULRULUDRDRURLLDUUDURLUUUDRRRLUUDRUUDDDRRRLRURLLLDDDRDDRUDDULUULDDUDRUUUDLRLLRLDUDUUUUDLDULLLDRLRLRULDDDDDLULURUDURDDLLRDLUDRRULDURDDLUDLLRRUDRUDDDLLURULRDDDRDRRLLUUDDLLLLRLRUULRDRURRRLLLLDULDDLRRRRUDRDULLLDDRRRDLRLRRRLDRULDUDDLDLUULRDDULRDRURRURLDULRUUDUUURDRLDDDURLDURLDUDURRLLLLRDDLDRUURURRRRDRRDLUULLURRDLLLDLDUUUDRDRULULRULUUDDULDUURRLRLRRDULDULDRUUDLLUDLLLLUDDULDLLDLLURLLLRUDRDLRUDLULDLLLUDRLRLUDLDRDURDDULDURLLRRRDUUDLRDDRUUDLUURLDRRRRRLDDUUDRURUDLLLRRULLRLDRUURRRRRLRLLUDDRLUDRRDUDUUUDRUDULRRULRDRRRDDRLUUUDRLLURURRLLDUDRUURDLRURLLRDUDUUDLLLUULLRULRLDLRDDDU
DRRRDRUDRLDUUDLLLRLULLLUURLLRLDRLURDRDRDRLDUUULDRDDLDDDURURUDRUUURDRDURLRLUDRRRDURDRRRDULLRDRRLUUUURLRUULRRDUDDDDUURLDULUDLLLRULUDUURRDUULRRDDURLURRUDRDRLDLRLLULULURLRDLRRRUUURDDUUURDRDRUURUDLULDRDDULLLLLRLRLLUDDLULLUDDLRLRDLDULURDUDULRDDRLUDUUDUDRLLDRRLLDULLRLDURUDRLRRRDULUUUULRRLUDDDLDUUDULLUUURDRLLULRLDLLUUDLLUULUULUDLRRDDRLUUULDDRULDRLURUURDLURDDRULLLLDUDULUDURRDRLDDRRLRURLLRLLLLDURDLUULDLDDLULLLRDRRRDLLLUUDDDLDRRLUUUUUULDRULLLDUDLDLURLDUDULRRRULDLRRDRUUUUUURRDRUURLDDURDUURURULULLURLLLLUURDUDRRLRRLRLRRRRRULLDLLLRURRDULLDLLULLRDUULDUDUDULDURLRDLDRUUURLLDLLUUDURURUD
UDUUUUURUDLLLRRRDRDRUDDRLLDRRLDRLLUURRULUULULRLLRUDDRLDRLUURDUDLURUULLLULLRRRULRLURRDDULLULULRUDDDUURDRLUDUURRRRUUULLRULLLDLURUDLDDLLRRRULDLLUURDRRRDRDURURLRUDLDLURDDRLLLUUDRUULLDLLLLUUDRRURLDDUDULUDLDURDLURUURDUUUURDLLLRUUURDUUUDLDUDDLUDDUDUDUDLDUDUUULDULUURDDLRRRULLUDRRDLUDULDURUURULLLLUDDDLURURLRLRDLRULRLULURRLLRDUDUDRULLRULRUDLURUDLLDUDLRDRLRDURURRULLDDLRLDDRLRDRRDLRDDLLLLDUURRULLRLLDDLDLURLRLLDULRURRRRDULRLRURURRULULDUURRDLURRDDLDLLLRULRLLURLRLLDDLRUDDDULDLDLRLURRULRRLULUDLDUDUDDLLUURDDDLULURRULDRRDDDUUURLLDRDURUDRUDLLDRUD
ULRDULURRDDLULLDDLDDDRLDUURDLLDRRRDLLURDRUDDLDURUDRULRULRULULUULLLLDRLRLDRLLLLLRLRRLRLRRRDDULRRLUDLURLLRLLURDDRRDRUUUDLDLDRRRUDLRUDDRURRDUUUDUUULRLDDRDRDRULRLLDLDDLLRLUDLLLLUURLDLRUDRLRDRDRLRULRDDURRLRUDLRLRLDRUDURLRDLDULLUUULDRLRDDRDUDLLRUDDUDURRRRDLDURRUURDUULLDLRDUDDLUDDDRRRULRLULDRLDDRUURURLRRRURDURDRULLUUDURUDRDRLDLURDDDUDDURUDLRULULURRUULDRLDULRRRRDUULLRRRRLUDLRDDRLRUDLURRRDRDRLLLULLUULRDULRDLDUURRDULLRULRLRRURDDLDLLRUUDLRLDLRUUDLDDLLULDLUURRRLRDULRLRLDRLDUDURRRLLRUUDLUURRDLDDULDLULUUUUDRRULLLLLLUULDRULDLRUDDDRDRDDURUURLURRDLDDRUURULLULUUUDDLRDULDDLULDUDRU
LRLRLRLLLRRLUULDDUUUURDULLLRURLDLDRURRRUUDDDULURDRRDURLRLUDLLULDRULLRRRDUUDDRDRULLDDULLLUURDLRLRUURRRLRDLDUDLLRLLURLRLLLDDDULUDUDRDLRRLUDDLRDDURRDRDUUULLUURURLRRDUURLRDLLUDURLRDRLURUURDRLULLUUUURRDDULDDDRULURUULLUDDDDLRURDLLDRURDUDRRLRLDLRRDDRRDDRUDRDLUDDDLUDLUDLRUDDUDRUDLLRURDLRUULRUURULUURLRDULDLDLLRDRDUDDDULRLDDDRDUDDRRRLRRLLRRRUUURRLDLLDRRDLULUUURUDLULDULLLDLULRLRDLDDDDDDDLRDRDUDLDLRLUDRRDRRDRUURDUDLDDLUDDDDDDRUURURUURLURLDULUDDLDDLRUUUULRDRLUDLDDLLLRLLDRRULULRLRDURRRLDDRDDRLU";

        public enum Location : short
        {
            One,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            HexA,
            HexB,
            HexC,
            HexD
        }

        static void Main(string[] args)
        {
            var currentLocation = Location.Five;

            var locs = new List<Location>();

            foreach (var x in inputString.Split(new string[] {Environment.NewLine}, StringSplitOptions.None))
            {
                foreach (var instr in x.ToCharArray())
                {
                    currentLocation = SimpleMove(currentLocation, instr);
                }
                locs.Add(currentLocation);
            }
            Console.Out.WriteLine($"Simple: {string.Join(" ", locs)}");

            currentLocation = Location.Five;
            locs = new List<Location>();

            foreach (var x in inputString.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
            {
                foreach (var instr in x.ToCharArray())
                {
                    currentLocation = CrazyMove(currentLocation, instr);
                }
                locs.Add(currentLocation);
            }
            Console.Out.WriteLine($"Crazy: {string.Join(" ", locs)}");

            Console.In.ReadLine();
        }

        private static Location SimpleMove(Location currentLocation, char direction)
        {
            switch (currentLocation)
            {
                case Location.One:
                    if (direction == 'U')
                    {
                        return Location.One;
                    }
                    else if (direction == 'L')
                    {
                        return Location.One;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Four;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Two;
                    }
                    break;
                case Location.Two:
                    if (direction == 'U')
                    {
                        return Location.Two;
                    }
                    else if (direction == 'L')
                    {
                        return Location.One;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Five;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Three;
                    }
                    break;
                case Location.Three:
                    if (direction == 'U')
                    {
                        return Location.Three;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Two;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Six;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Three;
                    }
                    break;
                case Location.Four:
                    if (direction == 'U')
                    {
                        return Location.One;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Four;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Seven;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Five;
                    }
                    break;
                case Location.Five:
                    if (direction == 'U')
                    {
                        return Location.Two;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Four;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Eight;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Six;
                    }
                    break;
                case Location.Six:
                    if (direction == 'U')
                    {
                        return Location.Three;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Six;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Nine;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Five;
                    }
                    break;
                case Location.Seven:
                    if (direction == 'U')
                    {
                        return Location.Four;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Seven;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Seven;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Eight;
                    }
                    break;
                case Location.Eight:
                    if (direction == 'U')
                    {
                        return Location.Five;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Seven;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Eight;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Nine;
                    }
                    break;
                case Location.Nine:
                    if (direction == 'U')
                    {
                        return Location.Six;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Eight;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Nine;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Nine;
                    }
                    break;
            }
            return Location.Eight;
        }

        private static Location CrazyMove(Location currentLocation, char direction)
        {
            switch (currentLocation)
            {
                case Location.One:
                    if (direction == 'U')
                    {
                        return Location.One;
                    }
                    else if (direction == 'L')
                    {
                        return Location.One;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Three;
                    }
                    else if (direction == 'R')
                    {
                        return Location.One;
                    }
                    break;
                case Location.Two:
                    if (direction == 'U')
                    {
                        return Location.Two;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Two;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Six;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Three;
                    }
                    break;
                case Location.Three:
                    if (direction == 'U')
                    {
                        return Location.One;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Two;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Seven;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Four;
                    }
                    break;
                case Location.Four:
                    if (direction == 'U')
                    {
                        return Location.Four;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Three;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Eight;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Four;
                    }
                    break;
                case Location.Five:
                    if (direction == 'U')
                    {
                        return Location.Five;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Five;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Five;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Six;
                    }
                    break;
                case Location.Six:
                    if (direction == 'U')
                    {
                        return Location.Two;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Five;
                    }
                    else if (direction == 'D')
                    {
                        return Location.HexA;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Seven;
                    }
                    break;
                case Location.Seven:
                    if (direction == 'U')
                    {
                        return Location.Three;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Six;
                    }
                    else if (direction == 'D')
                    {
                        return Location.HexB;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Eight;
                    }
                    break;
                case Location.Eight:
                    if (direction == 'U')
                    {
                        return Location.Four;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Seven;
                    }
                    else if (direction == 'D')
                    {
                        return Location.HexC;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Nine;
                    }
                    break;
                case Location.Nine:
                    if (direction == 'U')
                    {
                        return Location.Nine;
                    }
                    else if (direction == 'L')
                    {
                        return Location.Eight;
                    }
                    else if (direction == 'D')
                    {
                        return Location.Nine;
                    }
                    else if (direction == 'R')
                    {
                        return Location.Nine;
                    }
                    break;
                case Location.HexA:
                    if (direction == 'U')
                    {
                        return Location.Six;
                    }
                    else if (direction == 'L')
                    {
                        return Location.HexA;
                    }
                    else if (direction == 'D')
                    {
                        return Location.HexA;
                    }
                    else if (direction == 'R')
                    {
                        return Location.HexB;
                    }
                    break;
                case Location.HexB:
                    if (direction == 'U')
                    {
                        return Location.Seven;
                    }
                    else if (direction == 'L')
                    {
                        return Location.HexA;
                    }
                    else if (direction == 'D')
                    {
                        return Location.HexD;
                    }
                    else if (direction == 'R')
                    {
                        return Location.HexC;
                    }
                    break;
                case Location.HexC:
                    if (direction == 'U')
                    {
                        return Location.Eight;
                    }
                    else if (direction == 'L')
                    {
                        return Location.HexB;
                    }
                    else if (direction == 'D')
                    {
                        return Location.HexC;
                    }
                    else if (direction == 'R')
                    {
                        return Location.HexC;
                    }
                    break;
                case Location.HexD:
                    if (direction == 'U')
                    {
                        return Location.HexB;
                    }
                    else if (direction == 'L')
                    {
                        return Location.HexD;
                    }
                    else if (direction == 'D')
                    {
                        return Location.HexD;
                    }
                    else if (direction == 'R')
                    {
                        return Location.HexD;
                    }
                    break;
            }
            return Location.Eight;
        }

    }
}