﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    class Program
    {
        enum Direction
        {
            North,
            West,
            South,
            East
        }

        private const string directions =
            @"L4, L1, R4, R1, R1, L3, R5, L5, L2, L3, R2, R1, L4, R5, R4, L2, R1, R3, L5, R1, L3, L2, R5, L4, L5, R1, R2, L1, R5, L3, R2, R2, L1, R5, R2, L1, L1, R2, L1, R1, L2, L2, R4, R3, R2, L3, L188, L3, R2, R54, R1, R1, L2, L4, L3, L2, R3, L1, L1, R3, R5, L1, R5, L1, L1, R2, R4, R4, L5, L4, L1, R2, R4, R5, L2, L3, R5, L5, R1, R5, L2, R4, L2, L1, R4, R3, R4, L4, R3, L4, R78, R2, L3, R188, R2, R3, L2, R2, R3, R1, R5, R1, L1, L1, R4, R2, R1, R5, L1, R4, L4, R2, R5, L2, L5, R4, L3, L2, R1, R1, L5, L4, R1, L5, L1, L5, L1, L4, L3, L5, R4, R5, R2, L5, R5, R5, R4, R2, L1, L2, R3, R5, R5, R5, L2, L1, R4, R3, R1, L4, L2, L3, R2, L3, L5, L2, L2, L1, L2, R5, L2, L2, L3, L1, R1, L4, R2, L4, R3, R5, R3, R4, R1, R5, L3, L5, L5, L3, L2, L1, R3, L4, R3, R2, L1, R3, R1, L2, R4, L3, L3, L3, L1, L2";

        private const string directions2 = "R8, R4, R4, R8";

        static void Main(string[] args)
        {
            var steps = directions.Split(',');

            var currentDirection = Direction.North;

            int x = 0, y = 0;

            var visitedLocations = new List<Place>();

            visitedLocations.Add(new Place {X = 0, Y = 0});

            bool shouldStop = false;

            foreach (var step in steps)
            {
                var trimStep = step.Trim();
                currentDirection = Turn(currentDirection, trimStep[0]);

                int distance = int.Parse(trimStep.Substring(1));

                for (int i = 0; i < distance; i++)
                {

                    switch (currentDirection)
                    {
                        case Direction.North:
                            y = y + 1;
                            break;
                        case Direction.South:
                            y = y - 1;
                            break;
                        case Direction.East:
                            x = x + 1;
                            break;
                        case Direction.West:
                            x = x - 1;
                            break;
                    }

                    var currentPlace = new Place {X = x, Y = y};
                    if (visitedLocations.FirstOrDefault(loc => loc.X == currentPlace.X && loc.Y == currentPlace.Y) ==
                        null)
                    {
                        Console.WriteLine(currentPlace);
                        visitedLocations.Add(currentPlace);
                    }
                    else
                    {
                        shouldStop = true;
                        Console.WriteLine("SHOULD STOP");
                        Console.WriteLine($"X: {x}, Y: {y}, Total: {x + y}");
                        break;
                    }
                }
                if (shouldStop)
                {
                    break;
                }
            }

            Console.WriteLine($"X: {x}, Y: {y}, Total: {x + y}");

            Console.ReadLine();
        }


        private static Direction Turn(Direction current, char option)
        {
            switch (current)
            {
                case Direction.North:
                    return option == 'R' ? Direction.East : Direction.West;
                case Direction.East:
                    return option == 'R' ? Direction.South : Direction.North;
                case Direction.South:
                    return option == 'R' ? Direction.West : Direction.East;
                case Direction.West:
                default:
                    return option == 'R' ? Direction.North : Direction.South;
            }
        }
    }

    public class Place
    {
        public int X { get; set; }
        public int Y { get; set; }

        public override string ToString()
        {
            return $"X: {X}, Y:{Y}";
        }
    }
}