﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Day5
{
    class Program
    {
        private const string sample = "abc";
        private const string inputString = "ffykfhsq";

        static void Main(string[] args)
        {
            var hasher = new Hasher();

            char[] passwordStr = "********".ToCharArray();

            for (int i = 1; i < int.MaxValue; i++)
            {
                var hashAway = $"{inputString}{i}";
                var result = hasher.CalculateMD5Hash(hashAway);

                if (result.StartsWith("00000"))
                {
                    var position = (int) char.GetNumericValue(result[5]);
                    var value = result[6];


                    if (position >= 0 && position < 8&& passwordStr[position] == '*')
                    {
                        passwordStr[position] = value;
                        Console.WriteLine("swap");
                    }

                    Console.WriteLine($"{value} to {position} - {new string(passwordStr)}");
                }

                if (!(new string(passwordStr).Contains("*")))
                {
                    break;
                }
            }

            Console.Out.WriteLine(passwordStr);
            Console.In.ReadLine();
        }
    }


    public class Hasher
    {
        private MD5 md5 = System.Security.Cryptography.MD5.Create();

        public string CalculateMD5Hash(string input)

        {
            // step 1, calculate MD5 hash from input

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)

            {
                sb.Append(hash[i].ToString("X2").ToLower());
            }

            return sb.ToString();
        }
    }
}