﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day12
{
    class Program
    {
        private static readonly string Instructions = @"cpy 1 a
cpy 1 b
cpy 26 d
jnz c 2
jnz 1 5
cpy 7 c
inc d
dec c
jnz c -2
cpy a c
inc a
dec b
jnz b -2
cpy c b
dec d
jnz d -6
cpy 16 c
cpy 12 d
inc a
dec d
jnz d -2
dec c
jnz c -5";

        private static readonly Dictionary<string, int> Registers = new Dictionary<string, int>
        {
            {"a", 0},
            {"b", 0},
            {"c", 1},
            {"d", 0}
        };

        static void Main(string[] args)
        {
            var index = 0;
            var part2 = false;
            if (part2)
            {
                Registers["c"] = 1;
            }
            Stopwatch sw = Stopwatch.StartNew();

            var operations = Instructions.Split(Environment.NewLine.ToCharArray(),
                StringSplitOptions.RemoveEmptyEntries);

            while (index < operations.Count())
            {
                string[] parts = operations[index].ToLower()
                    .Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                var nextStep = true;
                switch (parts[0])
                {
                    case "cpy":

                        // Copy value
                        // Is value hardcoded or the contents of a register?
                        int value;
                        if (!int.TryParse(parts[1], out value))
                        {
                            // Use contents of register.
                            value = Registers[parts[1]];
                        }
                        Registers[parts[2]] = value;

                        break;
                    case "inc":

                        // Increase value of register by 1
                        Registers[parts[1]]++;
                        break;
                    case "dec":

                        // Decrease value of register by 1
                        Registers[parts[1]]--;
                        break;
                    case "jnz":

                        // Jump if not zero
                        // If register does not exist, use literal value
                        int registerValue = Registers.ContainsKey(parts[1])
                            ? Registers[parts[1]]
                            : Convert.ToInt32(parts[1]);
                        if (registerValue != 0)
                        {
                            index += Convert.ToInt32(parts[2]);
                            nextStep = false;
                        }

                        break;
                }
                if (nextStep)
                {
                    index++;
                }
            }
            sw.Stop();
            DisplayRegisters();
            Console.WriteLine($"Finished. Total time: {new TimeSpan(sw.ElapsedTicks):g}");
            Console.ReadKey();
        }

        private static void DisplayRegisters()
        {
            Console.Clear();
            Console.WriteLine("Register values:");
            foreach (KeyValuePair<string, int> register in Registers)
            {
                Console.WriteLine($"{register.Key.PadRight(5)}{register.Value.ToString().PadLeft(5)}");
            }
        }
    }
}