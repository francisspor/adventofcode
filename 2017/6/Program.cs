﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _6
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var input = @"4	1	15	12	0	9	9	5	5	8	7	3	14	5	12	3";

            var values = new List<int>();
            var history = new Dictionary<string, int>();

            foreach (var i in input.Split('\t')) {
                values.Add(int.Parse(i));    
            }
            history.Add(string.Join(",",input), 1);

            var count = 0;
            while(true) {
                count++;
                var max = values.Max();
                var maxIndex = values.IndexOf(max);

                Console.WriteLine($"{max} @ {maxIndex}");

                values[maxIndex] = 0;

                while (max > 0) {
                    maxIndex+=1;
                    if (maxIndex >= values.Count) {
                        maxIndex = 0;
                    }
                    values[maxIndex] = values[maxIndex] + 1;
                    max--;
                }
                var key = string.Join(",", values);
                if (!history.Keys.Contains(key)) {
                    history.Add(key, 1);
                } else {
                    history[key] = history[key] + 1;
                }

                Console.WriteLine($"{history.Count}");

                Console.WriteLine($"{history.Where(x=>x.Value > 1).Count()}");
            }
        }

    }
    
}
